function validateForm()
//NURUL AINA RAHA BINTI AMRAN
//DATE: 27/6/2021
{
	var yourname = document.forms["form"]["yourname"].value;
	var question1 = document.forms["form"]["question1"].value;
	var question2 = document.forms["form"]["question2"].value;
	var question3 = document.forms["form"]["question3"].value;
	var question4 = document.forms["form"]["question4"].value;
	var question5 = document.forms["form"]["question5"].value;
	var question6 = document.forms["form"]["question6"].value;
	var question7 = document.forms["form"]["question7"].value;
	var question8 = document.forms["form"]["question8"].value;
	var question9= document.forms["form"]["question9"].value;
	var question10 = document.forms["form"]["question10"].value;
	var score = 0;

	if(yourname=="")
	{
		alert("Name is required");
		return false;
	}

	if(question1=="")
	{
		alert("Oops!! Question 1 is required");
		return false;
	}

	if(question2=="")
	{
		alert("Oops!! Question 2 is required");
		return false;
	}

	if(question3=="")
	{
		alert("Oops!! Question 3 is required");
		return false;
	}

	if(question4=="")
	{
		alert("Oops!! Question 4 is required");
		return false;
	}

	if(question5=="")
	{
		alert("Oops!! Question 5 is required");
		return false;
	}

	if(question6=="")
	{
		alert("Oops!! Question 6 is required");
		return false;
	}

	if(question7=="")
	{
		alert("Oops!! Question 7 is required");
		return false;
	}

	if(question8=="")
	{
		alert("Oops!! Question 8 is required");
		return false;
	}

	if(question9=="")
	{
		alert("Oops!! Question 9 is required");
		return false;
	}

	if(question10=="")
	{
		alert("Oops!! Question 10 is required");
		return false;
	}

	if(question1=="Cascading Style Sheets")
	{
		score=score+1;
	}

	if(question2=="style")
	{
		score=score+1;
	}

	if(question3=="background-color")
	{
		score=score+1;
	}

	if(question4=="color")
	{
		score=score+1;
	}

	if(question5=="font-size")
	{
		score=score+1;
	}

	if(question6=="weight:bold;")
	{
		score=score+1;
	}

	if(question7=="Seperate each selector with a comma")
	{
		score=score+1;
	}

	if(question8=="static")
	{
		score=score+1;
	}

	if(question9=="#demo")
	{
		score=score+1;
	}

	if(question10=="No")
	{
		score=score+1;
	}

	if(score<=4)
	{
		alert("Keep trying, "+yourname+ "! You answered "+score+ " out of 10 correctly");
	}
	else if(score<=9)
	{
		alert("Way to go, " +yourname+ " You got " +score+ " out of 10 correctly");
	}
	else
	{
		alert("Congratulation " +yourname+ "! You got " +score+ " out of 10");
	}
}